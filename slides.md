---
# vim: set filetype=revealjs:
# vim: set syntax=markdown:
title: ":package: Docker :whale:"
subtitle: "Multistage + distroless go *brrrrr*"
author: "[Pablo COVES](https://pcoves.gitlab.io)"
date: "
Human Talks :: 2023-05-09
<br />
<img src='./licence.png' style='height: 2em;' />
"
---

# Virtualization VS Containerization

## Virtualization

<img src="./virtualization.svg" style="height: 50vh;" />

## Containerization

<img src="./containerization.svg" style="height: 50vh;" />

# Docker

* :warning: *Daemon* with `root` privileges
* :building_construction: `Build` to create an image
* :runner: `Run` to play wit it
* :pushpin: `Push` to share with the world

## Dockerfile

```dockerfile
FROM $image:$version

COPY $host $container

RUN $commands

ENTRYPOINT $program
CMD $arguments
```

## Base image

```dockerfile
FROM rust:latest

COPY . /demo
WORKDIR /demo

RUN cargo build --release

ENTRYPOINT ["/demo/target/release/demo"]
```

> 1.4GB

## Multistage

```dockerfile
FROM rust:latest as build
COPY . /demo
WORKDIR /demo
RUN cargo build --release

FROM debian:bookworm-slim

COPY --from=build /demo/target/release/demo /usr/local/bin/demo

ENTRYPOINT ["/usr/local/bin/demo"]
```

> 75MB

## Distroless

```dockerfile
FROM rust:latest as build
COPY . /demo
WORKDIR /demo
RUN cargo build --release

FROM gcr.io/distroless/cc

COPY --from=build /demo/target/release/demo ./demo

ENTRYPOINT ["./demo"]
```

> 23.1MB

## Scratch

```dockerfile
FROM rust:latest as build
COPY . /demo
WORKDIR /demo

RUN rustup target add x86_64-unknown-linux-musl
RUN cargo install --target x86_64-unknown-linux-musl --path .

FROM scratch
COPY --from=build /usr/local/cargo/bin/demo ./demo
ENTRYPOINT ["./demo"]
```

> 412kB

# Conclusion

* Smaller assets (and by a lot!)
* Better security (and by a lot!)
* No silver bullet

## Questions ?

<img src="./qr-code.png" style="height: 30vh" />

[https://pcoves.gitlab.io/human-talks_docker](https://pcoves.gitlab.io/human-talks_docker)
